package dev.maven.multimodule.application

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["dev.maven.multimodule"])
class AppConfiguration