package dev.maven.multimodule.application

import dev.maven.multimodule.library.Library
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@RestController
class DemoApplication {
    @GetMapping("/")
    fun getStuff(@Autowired library: Library): String {
        return library.formatGreetingMessage("Blade")
    }
}

fun main(args: Array<String>) {

    val lib = dev.maven.demo.Library()
    lib.printDep()

    runApplication<DemoApplication>(*args)
}

