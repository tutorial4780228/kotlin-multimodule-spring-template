package dev.maven.multimodule.library

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [Library::class])
class LibraryTest (@Autowired private val library: Library) {
    @Test
    fun testGreeting() {
        library.printGreetingMessage("Blade Wolfmoon")
    }
}