package dev.maven.multimodule.library

import org.springframework.stereotype.Component

@Component
class Library {
    fun printGreetingMessage(name: String) {
        println("Dear, $name. Hi!")
    }

    fun formatGreetingMessage(name: String): String {
        return "Dear, $name. Hi!"
    }
}